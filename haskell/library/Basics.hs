
module Basics
    ( hexToB64
    , fixedXor
    ) where 

import Control.Applicative (liftA2)
import qualified Data.Bits                   as Bit
import qualified Data.Hex                    as Hex
import qualified Data.ByteString.Lazy        as BL
import qualified Data.ByteString.Base64.Lazy as B64


unhexMaybe :: BL.ByteString -> Maybe BL.ByteString
unhexMaybe = Hex.unhex

hexToB64 :: BL.ByteString -> Maybe BL.ByteString
hexToB64 = fmap B64.encode . unhexMaybe
            

fixedXor :: BL.ByteString -> BL.ByteString -> Maybe BL.ByteString
fixedXor lhs rhs = Hex.hex <$> liftA2 _fixedXor (_decode lhs) (_decode rhs)
    where _decode = fmap BL.unpack . unhexMaybe
          _fixedXor leftWords rightWords =
                BL.pack [ xi `Bit.xor` yi
                        | (xi, yi) <- zip leftWords rightWords
                        ]


sbXorCipher :: BL.ByteString -> [ByteString]
sbXorCipher = sbXorCipher
