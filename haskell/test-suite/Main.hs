

import qualified Test.Tasty
import Test.Tasty.Hspec
import qualified Basics


hexToB64 :: Expectation
hexToB64 = Basics.hexToB64 hexIn `shouldBe` Just b64Out
    where hexIn  = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
          b64Out = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"


fixedXor :: Expectation
fixedXor = Basics.fixedXor lhsHex rhsHex `shouldBe` Just hexOut
    where lhsHex = "1c0111001f010100061a024b53535009181c"
          rhsHex = "686974207468652062756c6c277320657965"
          hexOut = "746865206B696420646F6E277420706C6179"

spec :: Spec
spec = parallel $ do
    it "set1 problem 1" hexToB64
    it "set1 problem 2" fixedXor
        

main :: IO ()
main = do
    test <- testSpec "cryptopals" spec
    Test.Tasty.defaultMain test
